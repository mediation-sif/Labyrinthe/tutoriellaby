%
%  untitled
%
%  Created by Sebastien Gilles on 2013-05-16.
%  Copyright (c) 2013 Inria. All rights reserved.
%
\documentclass[a4paper]{article}

% Use utf-8 encoding for foreign characters
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[francais]{babel}
\usepackage[top=0.75in, bottom=1.25in, left=1.25in, right=1.25in]{geometry}

% Uncomment some of the following if you use the features
%
% Running Headers and footers
%\usepackage{fancyhdr}

% Multipart figures
%\usepackage{subfigure}

% More symbols
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{latexsym}
\usepackage{gensymb}

% Surround parts of graphics with box
\usepackage{boxedminipage}

% Package for including code in the document
\usepackage{listings}
\usepackage{verbatim}
\usepackage{color}
\usepackage{indentfirst}
\usepackage{enumitem}

% Package for biblio
\usepackage[authoryear,round,comma]{natbib}
\usepackage[colorlinks,linkcolor=blue,citecolor=blue,pagebackref]{hyperref}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\ttfamily,        % the size of the fonts that are used for the code
  belowskip=-10pt,             % added to limit the spaces below the code sampling. Can't understand why it has to be negative!
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=C++,                    % the language of the code
  literate=
  {á}{{\'a}}1
  {à}{{\`a}}1
  {ã}{{\~a}}1
  {é}{{\'e}}1
  {è}{{\`e}}1
  {ê}{{\^e}}1
  {í}{{\'i}}1
  {ó}{{\'o}}1
  {õ}{{\~o}}1
  {ú}{{\'u}}1
  {ü}{{\"u}}1
  {ç}{{\c{c}}}1,
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}





% If you want to generate a toc for each chapter (use with book)
\usepackage{minitoc}

% This is now the recommended way for checking for PDFLaTeX:
\usepackage{ifpdf}

%\newif\ifpdf
%\ifx\pdfoutput\undefined
%\pdffalse % we are not running PDFLaTeX
%\else
%\pdfoutput=1 % we are running PDFLaTeX
%\pdftrue
%\fi

\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi



\title{Initiation à la programmation en C avec Laby\footnote{\url{https://sgimenez.github.io/laby/}}}
\author{Equipe de médiation du centre Inria Saclay}
\date{}
%\date{\vspace{-3.ex}}

\begin{document}
	
\ifpdf
\DeclareGraphicsExtensions{.pdf, .jpg, .tif}
\else
\DeclareGraphicsExtensions{.eps, .jpg}
\fi

\maketitle


\section{Raccourci clavier utile~: copier/coller}

Dans le programme, on appelle souvent les mêmes instructions; pour éviter de les retaper à chaque fois, on peut utiliser les raccourcis clavier pour copier et coller un bout de texte~:
\begin{itemize}[itemsep=1pt]
    \item Surligner avec la souris ou \textit{Shift} et les flèches de direction du clavier le texte à copier.
    \item Appuyer sur \textit{Ctrl} et \textit{C} pour indiquer que vous voulez copier ce texte en mémoire.
    \item Aller à l'endroit où il est requis et taper \textit{Ctrl} et \textit{V}.
\end{itemize}

\section{Les instructions Laby}\label{SecInstructions}

\begin{itemize}[itemsep=1pt]
    \item \lstinline{droite()}~: tourner de 90\degree  \ vers la droite.
    \item \lstinline{gauche()}~: tourner de 90\degree  \ vers la gauche.
    \item \lstinline{avance()}~: avance d'une case.
    \item \lstinline{prend()}~: soulève le rocher devant la fourmi et le met sur son dos.
    \item \lstinline{pose()}~: pose le rocher sur la case que voit la fourmi.
    \item \lstinline{regarde()}~: la fourmi dit ce qu'elle voit devant elle. Cette fonction est différente des autres car elle a une \textbf{valeur de retour}~: elle renvoie une information qu'on peut ensuite utiliser par exemple pour écrire une condition. Les valeurs possibles sont (en respectant la majuscule): \lstinline{Vide}, \lstinline{Mur}, \lstinline{Caillou}, \lstinline{Toile}, \lstinline{Sortie} et \lstinline{Inconnu}.
    \item \lstinline{ouvre()}~: ouvre la porte de sortie devant la fourmi (ce qui finit le niveau).
    \item \lstinline{dit("Hello world!")}~: affiche le texte donné entre guillemets sur l'interface de dialogue.

\end{itemize}

\section{Quelques règles de syntaxe du langage C}

\begin{itemize}[itemsep=1pt]
    \item Toutes les lignes d'instruction doivent se terminer par un \lstinline{;}.    
    \item Tout bloc d'instructions commence et finit par des accolades \lstinline|{}|. En particulier, la fonction \lstinline{fourmi()} doit conserver son accolade de fermeture.
    \item Pour comparer des valeurs (on aura besoin de ça pour la fonction \textbf{regarde()}), on utilise les symboles suivants:
    \begin{itemize}[itemsep=1pt]
        \item Égalité~: \lstinline{n == 5} (noter le double égal~!)
        \item Inégalité~: \lstinline{n != 5} (en maths on écrit plutôt $n \ne 5$)
        \item Plus grand que~: \lstinline{n > 5}
        \item Plus petit que ou égal à~: \lstinline{n <= 5}
    \end{itemize}
\end{itemize}

\section{Notions abordées dans les exercices}

\subsection{A partir de 2.a: la boucle "Tant que"}

Une boucle permet de répéter une ou plusieurs action(s) aussi longtemps qu'une condition reste vraie. Par exemple:

\begin{lstlisting}[]
    while (regarde() != Toile)
    { 
        avance();
    }
\end{lstlisting}

fait avancer la fourmi tant qu'elle n'a pas de toile devant
elle. À noter que la boucle ne s'arrête jamais si elle a un mur devant
elle; il faut donc être vigilant d'écrire des conditions qui deviennent fausses à un moment ou le programme ne s'arrêtera jamais~!


\subsection{A partir de 2.c: écrire de nouvelles instructions}

On peut enseigner à la fourmi de nouvelles instructions en combinant des instructions existantes~:

\begin{lstlisting}[]
    void avance_2_cases()
    { 
        avance();        
        avance();
    }
\end{lstlisting}

On dispose alors d'une nouvelle instruction \lstinline{avance_2_cases} qui fait avancer le robot de deux cases et qui a exactement le même statut que celles définies précédemment dans la section \ref{SecInstructions}.

\textbf{Note~:} En C, les fonctions ne peuvent pas être créées à l'intérieur d'une autre fonction~; \lstinline{avance_2_cases()} doit donc être positionnée avant la fonction \lstinline{fourmi()}.

\subsection{A partir de 3.a: tester une condition}

Parfois, on ne veut faire une action que si une condition donnée est respectée~: par exemple on ne veut pas que la fourmi avance s'il y a une toile d'araignée devant lui. On utilise alors l'instruction de test \lstinline{if} (les parenthèses qui entourent la condition sont obligatoires)~:


\begin{lstlisting}[]
    if (regarde() != Toile)
    { 
        avance(); 
    }
\end{lstlisting}


\subsection{A partir de 'counting the rocks': boucle itérative}

Il existe un autre type de boucle nommée \textbf{boucle itérative} ou le plus souvent simplement \textbf{boucle for}, qui prend trois arguments (tous facultatifs):

\begin{lstlisting}[]
    for (condition initiale; condition arret; operation de fin de boucle)
    { ... }
\end{lstlisting}

Une utilisation très courante est lorsque l'on veut faire une opération un certain nombre de fois~:

\begin{lstlisting}[]
    for (int i = 0; i < 5; i += 1) // `int` sert à déclarer une variable 
    {  
        avance();
    }
\end{lstlisting}

qui fait avancer de 5 cases la fourmi.


\end{document}
